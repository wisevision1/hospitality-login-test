import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../user';
import { JwtResponse } from './jwt-response';
import { tap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor() { }
  public login(userInfo: User) {
    localStorage.setItem('ACCESS_TOKEN', userInfo.email);
  }

  public sendemail(userInfo: User) {
    localStorage.getItem( userInfo.email);
  }

  public isLoggedIn() {
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  public logout() {
    localStorage.removeItem('ACCESS_TOKKEN');
    localStorage.removeItem('ACCESS_TOKEN');
    console.log(123, localStorage);
  }
}
