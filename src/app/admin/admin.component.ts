import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { Sender } from '../api.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { User } from '../user';
import { DatePipe } from '@angular/common';
import { TranslateService } from '../translate.service';
import { QRCodeComponent } from '../qrcode/qrcode.component';
import { last, first } from '../../../node_modules/rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss', './hamburgers.scss'],
  providers: [Sender]
})
export class AdminComponent implements OnInit {
  @ViewChild('name') name: ElementRef;
  @ViewChild('lastName') lastName: ElementRef;
  @ViewChild('patronymic') patronymic: ElementRef;
  @ViewChild('email') email: ElementRef;
  @ViewChild('phone') phone: ElementRef;
  @ViewChild('clickDoc') clickDoc: ElementRef;
  @ViewChild('menuToggle') menuToggle: ElementRef;


  constructor(private authService: AuthService,
    private router: Router,
    private sender: Sender,
    private fb: FormBuilder,
    private translate: TranslateService) {
    translate.use('ua').then(() => {
      console.log(translate.data);
    });
  }
  uk: boolean = true;
  menu: boolean = false;
  changeIconName: boolean = true;
  confrimIconName: boolean = false;
  changeIconLastName: boolean = true;
  confirmIconLastName: boolean = false;
  changeIconPatronymic: boolean = true;
  confirmIconPatronymic: boolean = false;
  changeIconPhone: boolean = true;
  confirmIconPhone: boolean = false;
  changeIconEmail: boolean = true;
  confirmIconEmail: boolean = false;
  local;
  userId;
  userCode;
  array;
  element;
  check;

  person = {
    firstname: 'default',
    lastname: 'default',
    patronymic: 'default',
    birthdate: '',
    mobilephone: 'default',
    email: 'default'
  };

  persondiscount = {
    group: <any>'',
    code: 'default',
    discount: 'default',
    validity: 'default',
    bonussum: 'default',
    paysum: 'default',
    credit: 'default',
    accumulation: 'default',
    extramoneysum: 'default'
  };

  mesForm = new FormGroup({
    Text: new FormControl('')
  });
  // changeForm;

  changeForm = new FormGroup({
    ID: new FormControl(''),
    FirstName: new FormControl(''),
    LastName: new FormControl(''),
    Patronymic: new FormControl(''),
    MobilePhone: new FormControl(''),
    EMail: new FormControl(''),
  });

  nameForm = new FormGroup({
    FirstName: new FormControl(''),
    ID: new FormControl('')
  });

  changePassForm = new FormGroup({
    ID: new FormControl(''),
    OldPassword: new FormControl(''),
    NewPassword: new FormControl(''),
    ConfirmPassword: new FormControl('')
  });

  toggleMenu() {
    let nav = document.getElementById('nav');
    let hamburger = document.querySelector('.hamburger');
    nav.classList.toggle('active');
    hamburger.classList.toggle('is-active');
  }

  setLangUA(lang: 'ua') {
    this.uk = true;
    this.translate.use(lang);
    document.getElementById('patronymic').style.display = 'flex';
    document.getElementById('patronymic').style.flexWrap = 'wrap';
  }

  setLangEN(lang: 'en') {
    this.uk = false;
    this.translate.use(lang);
    document.getElementById('patronymic').style.display = 'none';
  }

  ngOnInit() {
    this.sender.getPropositions().subscribe(index => { this.array = index; });
    localStorage.getItem('ACCESS_TOKEN');
    this.infoUser();
    document.getElementById('ServioContainer').style.display = 'none';
    document.getElementById('ServioResult').style.background = '#080808bd';
    setInterval(styleKiller, 500);
    function styleKiller() {



    }



  }

  infoUser() {
    console.log(118, true);
    this.sender.getCabinetInfo(localStorage.getItem('ACCESS_TOKEN')).subscribe(responseCabinetData => {
      this.userId = responseCabinetData.person.id;
      this.changePassForm.value.ID = this.userId;
      this.person.firstname = responseCabinetData.person.firstname;
      this.person.lastname = responseCabinetData.person.lastname;
      this.person.patronymic = responseCabinetData.person.patronymic;
      this.person.birthdate = responseCabinetData.person.birthdate;
      this.person.email = responseCabinetData.person.email;
      this.person.mobilephone = responseCabinetData.person.mobilephone;

      this.changeForm = new FormGroup({
        ID: new FormControl(this.userId),
        FirstName: new FormControl(this.person.firstname),
        LastName: new FormControl(this.person.lastname),
        Patronymic: new FormControl(this.person.patronymic),
        MobilePhone: new FormControl(this.person.mobilephone),
        EMail: new FormControl(this.person.email),
      });

      this.nameForm = new FormGroup({
        ID: new FormControl(this.userId),
        FirstName: new FormControl(this.person.firstname)
      });

      this.persondiscount.group = responseCabinetData.persondiscount.group;
      this.persondiscount.code = responseCabinetData.persondiscount.code;
      this.persondiscount.discount = responseCabinetData.persondiscount.discount;
      this.persondiscount.validity = responseCabinetData.persondiscount.validity;
      this.persondiscount.bonussum = responseCabinetData.persondiscount.bonussum;
      this.persondiscount.paysum = responseCabinetData.persondiscount.paysum;
      this.persondiscount.credit = responseCabinetData.persondiscount.credit;
      this.persondiscount.accumulation = responseCabinetData.persondiscount.accumulation;
      this.persondiscount.extramoneysum = responseCabinetData.persondiscount.extramoneysum;
    });
  }

  popOn() {
    console.log(75, true);
  }

  popOff() {
    console.log(79, false);
  }

  messageForm() {
    document.getElementById('mesForm').style.display = 'block';
  }

  openMenu() {
    document.getElementById('welcomeUser').classList.add('addaptive-pop-menu');
    this.menu = !this.menu;
    if (this.menu === false) {
      document.getElementById('welcomeUser').classList.remove('addaptive-pop-menu');
    }
  }

  // closeMenu() {
  //   // document.getElementById('welcomeUser').style.marginTop = "0"
  //   this.menu = false;
  // }

  openEditName() {
    console.log('open');
    this.changeIconName = false;
    this.confrimIconName = true;
  }

  editName() {
    console.log('edit')
    if (this.name.nativeElement.classList.contains('ng-valid')) {
      this.sender.changeFirstName(this.changeForm.value).subscribe(res => {
        Swal.fire({
          type: 'success',
          title: 'Success',
          text: 'Your name has been changed'
        });
      })
    } else {
      this.infoUser()
      this.changeForm.controls['FirstName'].setValue(this.person.firstname);
    }
    this.changeIconName = true;
    this.confrimIconName = false;
  }

  cancelEditName() {
    console.log(194, 'cancel');
    this.infoUser()
    this.changeForm.controls['FirstName'].setValue(this.person.firstname);
    this.changeIconName = true;
    this.confrimIconName = false;
  }

  openEditLastName() {
    this.changeIconLastName = false;
    this.confirmIconLastName = true;
  }

  editLastName() {
    if (this.lastName.nativeElement.classList.contains('ng-valid')) {
      this.sender.changeLastName(this.changeForm.value).subscribe(res => {
        console.log(res);
        Swal.fire({
          type: 'success',
          title: 'Success',
          text: 'Your last name has been changed'
        });
      });
    } else {
      this.infoUser();
      this.changeForm.controls['LastName'].setValue(this.person.lastname);
    }
    this.changeIconLastName = true;
    this.confirmIconLastName = false;
  }

  cancelEditLastName() {
    this.infoUser()
    this.changeForm.controls['LastName'].setValue(this.person.lastname);
    this.changeIconLastName = true;
    this.confirmIconLastName = false;
  }

  openEditPatronymic() {
    this.changeIconPatronymic = false;
    this.confirmIconPatronymic = true;
  }

  editPatronymic() {
    if (this.patronymic.nativeElement.classList.contains('ng-valid')) {
      this.sender.changePatro(this.changeForm.value).subscribe(res => {
        console.log(res);
        console.log('edit' + 'true');
        Swal.fire({
          type: 'success',
          title: 'Success',
          text: 'Your patronymic has been changed'
        });
      });
    } else {
      this.infoUser()
      this.changeForm.controls['Patronymic'].setValue(this.person.patronymic);
    }
    this.changeIconPatronymic = true;
    this.confirmIconPatronymic = false;
  }

  cancelEditPatronymic() {
    this.infoUser();
    this.changeForm.controls['Patronymic'].setValue(this.person.patronymic);
    this.changeIconPatronymic = true;
    this.confirmIconPatronymic = false;
  }

  openEditPhone() {
    this.changeIconPhone = false;
    this.confirmIconPhone = true;
  }

  editPhone() {
    if (this.phone.nativeElement.classList.contains('ng-valid')) {
      this.sender.changePhone(this.changeForm.value).subscribe(res => {
        console.log(res);
        console.log('edit' + 'true');
        Swal.fire({
          type: 'success',
          title: 'Success',
          text: 'Your phone number has been changed'
        });
      });
    } else {
      this.infoUser()
      Swal.fire({
        type: 'error',
        title: 'Error',
        text: 'Incorrect phone number'
      });
      this.changeForm.controls['MobilePhone'].setValue(this.person.mobilephone);
    }
    this.changeIconPhone = true;
    this.confirmIconPhone = false;
  }

  cancelEditPhone() {
    this.infoUser();
    this.changeForm.controls['MobilePhone'].setValue(this.person.mobilephone);
    this.changeIconPhone = true;
    this.confirmIconPhone = false;
  }

  openEditEmail() {
    this.changeIconEmail = false;
    this.confirmIconEmail = true;
  }

  editEmail() {
    if (this.email.nativeElement.classList.contains('ng-valid')) {
      this.sender.checkMail(this.changeForm.value.EMail).subscribe(result => {
        this.check = result;
        if (this.check === false || (this.check === true && this.changeForm.value.EMail === this.person.email)) {
          Swal.fire({
            type: 'success',
            title: 'Your email data has been changed',
            text: 'Have a nice day'
          });
          this.sender.changeEmail(this.changeForm.value).subscribe(res => {
            if (res.message) {
              localStorage.ACCESS_TOKEN = this.changeForm.value.EMail;
              this.changeForm.controls['EMail'].setValue(this.person.email);
            }
          });
        } else if (this.check === true && !(this.local === this.person.email)) {
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'The email already exists'
          });
        }
      });
    } else {
      this.infoUser();
      this.changeForm.controls['EMail'].setValue(this.person.email);
    }
    this.changeIconEmail = true;
    this.confirmIconEmail = false;
  }

  cancelEditEmail() {
    this.infoUser();
    this.changeForm.controls['EMail'].setValue(this.person.email);
    this.changeIconEmail = true;
    this.confirmIconEmail = false;
  }

  openForm() {
    let nav = document.getElementById('nav');
    nav.classList.toggle('active');
    console.log(this.person);
    this.changeForm.value.FirstName = this.person.firstname;
    console.log(this.changeForm);
    this.menu = false;
    // document.getElementById('welcomeUser').classList.remove('addaptive-pop-menu');
    // document.getElementById('welcomeUser').style.display = 'none';
    document.getElementById('usercontent').style.display = 'none';
    document.getElementById('poster').style.display = 'none';
    document.getElementById('setting').style.display = 'flex';
    document.getElementById('myForm').style.display = 'block';
    document.getElementById('changePass').style.display = 'block';
    console.log(document.getElementById('menu__toggle'));


    // document.getElementById('menu__toggle').setAttribute('checked', 'off');
    // let menu = document.getElementsByClassName('hamburger-menu') as HTMLCollectionOf<HTMLElement>;
    // menu[0].style.display = "none"
    // for (let i in menu) {
    //   menu[i].style.display = "none";
    // }
  }

  closeForm() {
    document.getElementById('usercontent').style.display = 'block';
    document.getElementById('poster').style.display = 'block';
    document.getElementById('setting').style.display = 'none';
    document.getElementById('myForm').style.display = 'none';
    document.getElementById('changePass').style.display = 'none';
    let menu = document.getElementsByClassName('hamburger-menu') as HTMLCollectionOf<HTMLElement>;
    menu[0].style.display = 'block';

    // this.changeForm.reset();
  }

  closeMesForm() {
    document.getElementById('mesForm').style.display = 'none';
    this.mesForm.reset();
  }


  openBooking() {
    document.getElementById('ServioContainer').style.display = 'block';
    document.getElementById('usercontent').style.display = 'none';
    document.getElementById('poster').style.display = 'none';
    document.getElementsByClassName('rkz-main-modal')[0].setAttribute('style', 'display:none');
    document.getElementsByClassName('footer')[0].setAttribute('style', 'display:none');
    document.getElementsByClassName('hotel-description')[0].setAttribute('style', 'color:white');
    document.getElementsByClassName('category-title-text')[0].setAttribute('style', 'color:white');
    document.getElementsByClassName('progress-text')[0].setAttribute('style', 'color:white');
  }


  openBookingTables() {
    document.getElementById('bookingTables').style.display = 'block';
    document.getElementById('usercontent').style.display = 'none';
    document.getElementById('poster').style.display = 'none';
  }

  sendMes() {
    this.sender.sendMessage(this.mesForm.value).subscribe(res => {
    });
  }

  // clickOnDoc(e) {
  //   // var div = document.getElementsByClassName('navigation') as HTMLCollectionOf<HTMLElement>;
  //   console.log(393, this.menuToggle);
  //   console.log(394, e.target)
  //   console.log(395, !this.menuToggle)
  //   console.log(396, !this.menuToggle === e.target)
  //   console.log(396, this.menuToggle.nativeElement.contains(e.target))

  //   let nav = document.getElementById('nav');
  //   // nav.classList.toggle('active');
  //   if (nav.classList.contains('active')) {
  //     if (!this.menuToggle.nativeElement.contains(e.target)) {
  //       let nav = document.getElementById('nav');
  //       nav.classList.toggle('active');
  //     }
  //   }
  // }

  sendNewData() {
    this.sender.checkMail(this.changeForm.value.EMail).subscribe(result => {
      this.check = result;
      this.changeForm.value.ID = this.userId;
      this.local = this.changeForm.value.EMail;

      if (this.name.nativeElement.classList.contains('ng-valid') &&
        this.lastName.nativeElement.classList.contains('ng-valid') &&
        this.email.nativeElement.classList.contains('ng-valid') &&
        this.phone.nativeElement.classList.contains('ng-valid')
      ) {
        if (this.check === false || (this.check === true && this.changeForm.value.EMail === this.person.email)) {
          localStorage.setItem('ACCESS_TOKEN', this.local);
          // document.getElementById('myForm').style.display = 'none';
          Swal.fire({
            type: 'success',
            title: 'Your profile data was changed',
            text: 'Have a nice day'
          });
          setTimeout(() => {
            window.location.reload();
          }, 3000);
          this.sender.changeCabinetData(this.changeForm.value).subscribe(response => {
          });
        } else if (this.check === true && !(this.local === this.person.email)) {
          Swal.fire({
            type: 'error',
            title: 'Error',
            text: 'The email already exists'
          });
        }
      } else if (!this.email.nativeElement.classList.contains('ng-valid')) {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'Incorrect email'
        });
      } else if (!this.phone.nativeElement.classList.contains('ng-valid')) {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'Incorrect phone number'
        });
      } else {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'Write in all inputs'
        });
      }
    });
  }

  changePass(confirmPassword) {
    console.log(235, confirmPassword);
    this.changePassForm.value.ID = this.changeForm.value.ID;
    console.log(this.changePassForm.value);
    if (this.changePassForm.value.OldPassword &&
      this.changePassForm.value.NewPassword &&
      confirmPassword) {
      if (this.changePassForm.value.NewPassword === confirmPassword) {
        this.sender.changePass(this.changePassForm.value).subscribe(index => {
          if (index.message) {
            Swal.fire({
              type: 'success',
              title: 'Your password was changed',
              text: 'Password changed successfull'
            });
          } else {
            Swal.fire({
              type: 'error',
              title: 'Password not changed',
              text: 'Invalid old password'
            });
          }
        });
      } else {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'Passwords do not match'
        });
      }
    } else {
      Swal.fire({
        type: 'error',
        title: 'Error',
        text: 'Fill in all the fields of the password change form'
      });
    }
    return false;
  }

  logout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }


}
