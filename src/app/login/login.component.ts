import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../user';
import { AuthService } from '../auth/auth.service';
import { Component, OnInit, ViewChild, ElementRef, } from '@angular/core';
import { TranslateService } from '../translate.service';
import { Sender } from '../api.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [Sender]
})

export class LoginComponent implements OnInit {

  @ViewChild('email') email: ElementRef;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private sender: Sender,
    private translate: TranslateService) {
      translate.use('ua').then(() => {
        console.log(translate.data);
      });
    }


  uk: boolean = true;
  loginForm: FormGroup;
  resetForm = new FormGroup({
    // MobilePhone: new FormControl(''),
    EMail: new FormControl(''),
    // NewPassword: new FormControl('')
  });
  isSubmitted = false;

  setLangUA(lang: 'ua') {
    this.uk = true;
    this.translate.use(lang);
    // document.getElementById('patronymic').style.display = 'flex';
    // document.getElementById('patronymic').style.flexWrap = 'wrap';
  }

  setLangEN(lang: 'en') {
    this.uk = false;
    this.translate.use(lang);
    // document.getElementById('patronymic').style.display = 'none';
  }

  
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  closeForm() {
    document.getElementById('myForm').style.display = 'none';
    document.getElementById('login').style.display = 'block';
    this.resetForm.reset();
  }

  openForm() {
    document.getElementById('myForm').style.display = 'block';
    document.getElementById('login').style.display = 'none';
  }
  get formControls() { return this.loginForm.controls; }

  redirect(){
    window.location.href ="https://loyalty.kavalier.com.ua"
  }
  sendNewData() {
    document.getElementById('myForm').style.display = 'none';
    document.getElementById('login').style.display = 'block';
    if (this.email.nativeElement.classList.contains('ng-valid')) {
      this.sender.resPas(this.resetForm.value.EMail).subscribe(response => {
        console.log(22222, response.hasOwnProperty("success"))
      if (response.hasOwnProperty("success") === true){
        Swal.fire({
          type: 'success',
          title: 'Check your email',
          text: 'A link with reset password was sent to your email'
        });
      } else {
        Swal.fire({
          type: 'error',
          title: 'Error',
          text: 'The Email may not exist, check if the Email entered correctly'
        });
      }
        // if (response.message === true) {
        //   Swal.fire({
        //     type: 'success',
        //     title: 'Your password was changed',
        //     text: 'Have a nice day'
        //   });
        // } else if (response.message === false) {
        //   Swal.fire({
        //     type: 'error',
        //     title: 'Error',
        //     text: 'Wrong profile data'
        //   });
        // }
      });
      this.resetForm.reset();
    } else {
      Swal.fire({
        type: 'error',
        title: 'Error',
        text: 'Wrong email format'
      });
    }
  }




  login() {
    this.sender.postRegistrationInfo(this.loginForm.value).subscribe(response => {
      if (response === (true)) {
        this.authService.login(this.loginForm.value);
        this.router.navigateByUrl('/admin');
      } else {
        this.loginForm.reset();
        Swal.fire({
          type: 'error',
          title: 'Помилка',
          text: 'Неправильно введений пароль або емейл'
        });
      }
    });
  }
}
