SBJ_DEBUG = true;

window.l = function(n) {
    if (SBJ_DEBUG) console.log(n);
}

var sbQ = null;
var header = document.getElementsByTagName('head')[0];
var registration_url = "https://sbj.rkz.io/uk/registration/";
var domain = "//sbj.rkz.io/";
var call_back_url = "https://sbj.rkz.io/call_back_form/";
var step_error_url = "https://sbj.rkz.io/uk/booking/step_error_generate/";
var modal_window = true;
var arrow_background = 'None';
var booking_module_id = '8';
var language = 'uk';
var content_el_id = 'None';
var content_el_class = 'None';
var body = document.querySelector('body');
var rooms_max_quantity = '5';
var room_adults_max_quantity = '4';
var room_childs_max_quantity = '3';
var session_id;
var session_url = "https://test.kavalier.com.ua";
var referrer = document.referrer;
var hotel_choice_click = 0;
var additional_services_url = "https://sbj.rkz.io/uk/additional_services/";
var ajax_context;

SBJ_MESSAGES = {
    'ERROR_TITLE': 'Помилка!',
    'ERROR_TEXT': 'Будь ласка, поновіть сторінку і спробуйте виконати дію ще раз.',
    'TIME_ERROR_TEXT': 'Будь ласка, поновіть сторінку і спробуйте ще раз.',
    'booking_start_error': 'Помилка JS функції при запуску пошуку',
    'booking_select_rooms_error': 'Помилка JS функції при виборі кімнат',
    'booking_user_profile_error': 'Помилка JS функції при заповненні даних користувача',
    'booking_additional_services_error': 'Помилка JS функції при виборі додаткових послуг',
    'booking_success_error': 'Помилка JS функції при успішному бронюванні',
    'servio_error_error': 'Помилка JS функції, що відстежує помилки',
    'child': 'дитина:',
    'adult': 'дорослий',
    'adults': 'дорослих',
    'zabronirovat': 'Забронювати',
    'vibrano': 'вибрано',
    'all_rooms_selected': 'Ви вибрали всі номери',
    'ostalsa': 'Залишився ',
    'ostalos': 'Залишилося ',
    'nomer': ' номер',
    'nomera': ' номери',
    'nomerov': ' номерів',
    'children_ages': {
        2: 'до 3-х років',
        3: '3 роки',
        4: '4 роки',
        5: '5 років',
        6: '6 років',
        7: '7 років',
        8: '8 років',
        9: '9 років',
        10: 'від 10 років'
    },
    'choice_type': 'Виберіть тип',
    'search_hotel': 'Знайти готель',
    'no results found': 'Результати не знайдені',
}

var STEPS_WITH_PRELOADER = {
    'hotel_rooms_content_generate': true,
    'registration_form_generate': true,
    'continue_after_registration': true,
    'continue_after_services': true,
    'continue_after_cart': true,
    'show_payment_form': true
};

style_list = [
    'static/css/select2.min.css',
    'static/css/jquery-ui.min.css',
    'static/css/my.css',
    'static/css/slick.css',
    'static/css/jquery.fancybox.min.css',
    'uk' + '/sb-color-theme.css?module_id=' + booking_module_id + '&session_url=' + session_url
];

var script1 = domain + "static/js/jquery-3.3.1.min.js";
var script2 = domain + "static/js/select2.full.min.js";
var script3 = domain + "static/js/jquery-ui.min.js";
var script4 = domain + "static/js/slick.min.js";
var script5 = domain + "static/js/jquery.fancybox.min.js";
var script6 = domain + "static/js/main.js";
var script7 = domain + 'static/js/datepicker-' + language + '.js';
script_dict = {[script1] : [script2, script3], [script3] : [script4, script5, script6, script7]};

for (var i = 0; i < style_list.length; ++i) {
    var link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');
    link.setAttribute('href', domain + style_list[i]);
    body.appendChild(link);
}

function script_loader(scripts_list) {
    for (var i = 0; i < scripts_list.length; ++i) {
        var script = document.createElement('script');
        script.src = scripts_list[i];
        body.appendChild(script);
        script.onload = function (el) {

            var url = el.target.src.replace(/http:|https:/, '');
            if (url === script6) createForm();
            var new_scripts_list = script_dict[el.target.src.replace(/http:|https:/, '')];
            if (new_scripts_list !== undefined) script_loader(new_scripts_list);
        };
    };
};
script_loader([script1]);

function users_sbj_functions(data) {
    booking_start = function() {
        console.log('Функция запустилась');
        try {
           eval( data['booking_start']);
        } catch(e) {
            console.log(SBJ_MESSAGES['booking_start_error']);
        }
    };

    booking_select_rooms = function() {
        try {
            eval(data['booking_select_rooms']);
        } catch(e) {
            console.log(SBJ_MESSAGES['booking_select_rooms_error']);
        }
    };

    booking_user_profile = function() {
        try {
            eval(data['booking_user_profile']);
        } catch(e) {
            console.log(SBJ_MESSAGES['booking_user_profile_error']);
        }
    };

    booking_additional_services = function() {
        try {
            eval(data['booking_additional_services']);
        } catch(e) {
            console.log(SBJ_MESSAGES['booking_additional_services_error']);
        }
    };

    booking_success = function() {
        try {
            eval(data['booking_success']);
        } catch(e) {
            console.log(SBJ_MESSAGES['booking_success_error']);
        }
    };

    servio_error = function() {
        try {
            eval(data['booking_error']);
        } catch(e) {
            console.log(SBJ_MESSAGES['booking_success_error']);
        }
    };
}

function createForm() {
    var divSearch = document.getElementById('ServioContainer');
    var divResult = document.getElementById('ServioResult');
    if(divSearch) {
        sbQ('#ServioContainer').html(
            '<div class="init-preloader-block">\
                <div class="align-center">\
                    <div class="init-preloader"></div><p>Триває завантаження модуля бронювання ...</p>\
                </div>\
            </div>'
        )
        var ls_data = JSON.parse(localStorage.getItem('servio_booking') || '{}');
        var booking_data = {
            "get_string": "?&module_id=8&lang=uk",
            'session_url': session_url,
            'referrer': referrer,
            'customer': ls_data['customer']
        };
        var url = "https://sbj.rkz.io/uk/main_page/";
        if ('?&amp;module_id=8&amp;lang=uk'.indexOf("hotel_id") !== -1) {
            booking_data['guests_list'] = ls_data['numbers_dict'] || {1: {'adults': 1, 'children': []}}
            booking_data['tourist_tax'] = ls_data['id_tourist_tax']
            booking_data['loyalty_code'] = ls_data['id_loyalty_code']

            if ('?&amp;module_id=8&amp;lang=uk'.indexOf("date_arrival") === -1) {
                booking_data['date_arrival'] = sbQ.datepicker.formatDate('yy-mm-dd', new Date(ls_data['date_arrival']));
                booking_data['date_departure'] = sbQ.datepicker.formatDate('yy-mm-dd', new Date(ls_data['date_departure']));
            } else {
                booking_data['time_arrival'] = ls_data['id_start_time']
                booking_data['time_departure'] = ls_data['id_end_time']
            }
        }
        sbQ.post(url, JSON.stringify(booking_data), function (data) {
            users_sbj_functions(data['user_functions']);
            start_content(data, divSearch, divResult);
            try {
                if (typeof window.sbj_onload  == 'function'){
                    window.sbj_onload();
                }
            } catch(e) {
                console.log('onload error');
            }
        });
    } else {
        l('Элемент для установки формы не найден.');
    }
}