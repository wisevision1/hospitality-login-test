import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Sender } from '../api.service';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { element } from '@angular/core/src/render3/instructions';
import { toArray } from 'rxjs/operators';
@Component({
  selector: 'app-booking-tables',
  templateUrl: './booking-tables.component.html',
  styleUrls: ['./booking-tables.component.scss'],
  providers: [Sender]
})

export class BookingTablesComponent implements OnInit {

  @ViewChild('datapick') datapick: ElementRef;
  @ViewChild('datapickTwo') datapickTwo: ElementRef;
  @ViewChild('description') description: ElementRef;


  first: boolean = true;
  second: boolean = false;
  third: boolean = false;
  fourth: boolean = false;
  fifth: boolean = false;
  six: boolean = false;
  drinks: boolean = true;
  firsCourse: boolean = false;
  secondCourse: boolean = false;
  desert: boolean = false;
  send;
  items = [];
  price = 0;

  // quantity: number = 0;
  valueItem;
  array;
  array1 = [];
  array2 = [];
  array3 = [];
  array4 = [];
  value;
  sendhardCode: object = {};
  token;
  getTablesJason;
  tables;
  places;
  simpleBookData;
  complexBookData;
  stolik = [];
  date;
  firstDate;
  secondDate;
  mealArray = [];
  bill;

  paymentForm = new FormGroup({
    PhoneNumber: new FormControl(''),
    Amount: new FormControl(''),
    PayID: new FormControl(''),
    CardCode: new FormControl(''),
    ExpMonth: new FormControl(''),
    ExpYear: new FormControl(''),
    CardCVV: new FormControl('')
  });

  constructor(private sender: Sender, private fb: FormBuilder) { }

  secondStep() {
    this.firstDate = moment(this.datapick.nativeElement.value).format('YYYY-MM-DD HH:mm:ss');
    this.secondDate = moment(this.datapickTwo.nativeElement.value).format('YYYY-MM-DD HH:mm:ss');
    if (this.firstDate !== 'Invalid date' && this.secondDate !== 'Invalid date' && this.firstDate < this.secondDate) {
      this.first = false;
      this.second = true;
      this.sendhardCode = {
        'CardCode': '53777',
        'TermID': '0987654321'
      };
      this.sender.terminalAuth(this.sendhardCode).subscribe(token => {
        console.log(5687, token)
        this.token = token;
      });
    } else {
      Swal.fire({
        type: 'error',
        title: 'Введіть правильну дату бронювання',
      });
    }
  }

  selectTable(event) {
    Swal.fire('Столик вибрано');
    let target = event.target;
    let idAttr = target.attributes.id;
    this.value = idAttr.nodeValue;
  }

  payment() {
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
    const stringLength = 10;
    let randomstring = '';
    for (let i = 0; i < stringLength; i++) {
      const rnum = Math.floor(Math.random() * chars.length);
      randomstring += chars.substring(rnum, rnum + 1);
    }
    console.log(99, this.price);
    this.paymentForm.value.Amount = this.price;
    this.paymentForm.value.PayID = randomstring;
    console.log(8888, this.paymentForm)
  
    this.sender.liqPayment(this.paymentForm.value).subscribe(response => {
      console.log(48, response)
      if (response === "success") {
        this.sender.bookTable(this.send).subscribe(response=>{
          const string = JSON.stringify(response);
            const resultParse = JSON.parse(string);
            this.bill = resultParse.BillID;
            console.log(44, resultParse.BillID);
            let billServioObj = {
            token: this.token,
            BillID: this.bill,
            Summ: this.price * 100,
            Description: this.paymentForm.value.CardCode + ' ' + 'Liqpay payment System'
            };
            this.sender.servioPayment(billServioObj).subscribe(response=> {
              console.log(66, response);
              });
        });
      } else if (response === "failure" ) {
          Swal.fire({
            type: "error",
            title: "Введені неправильні дані карточки",
          });
      }
    });
    console.log(36, this.paymentForm.value);
  }

  bookSimple() {
    this.simpleBookData = {
      'token': this.token,
      'SystemCode': '1010',
      'BillType': 4,
      'FirstDate': this.firstDate,
      'LastDate': this.secondDate,
      'UserName': 'CabinetUser',
      'PlaceCode': this.value,
      'EventID': '1',
      'EventName': '1',
    };
    this.sender.bookTable(this.simpleBookData).subscribe(response => {
      console.log(34 ,response)
      
      if (response.hasOwnProperty('Error') === true && response.hasOwnProperty('BillID') === false) {
        Swal.fire({
          title: 'На даний період столик недоступний',
          type: 'error'
        });
      } else { 
        const string = JSON.stringify(response);
        const resultParse = JSON.parse(string);
        this.bill = resultParse.BillID;
        console.log(44, resultParse.BillID);
        return response; }
    });
  }

  bookComplex() {
    this.complexBookData = {
      'token': this.token,
      'SystemCode': '1010',
      'BillType': 4,
      'FirstDate': this.firstDate,
      'LastDate': this.secondDate,
      'UserName': 'CabinetUser',
      'PlaceCode': this.value,
      'EventID': '1',
      'EventName': '1',
      'Items': this.mealArray
    }
  }

  incrementQuantity(event) {
    let target = event.target;
    let idAttr = target.attributes.id;
    this.valueItem = idAttr.nodeValue;
    let quantity = Number(document.getElementById(this.valueItem).getAttribute('value'));
    quantity += 1;
    document.getElementById(this.valueItem).setAttribute('value', `${quantity}`);
  }

  decrementQuantity(event) {
    let target = event.target;
    let idAttr = target.attributes.id;
    this.valueItem = idAttr.nodeValue;
    let quantity = Number(document.getElementById(this.valueItem).getAttribute('value'));
    if (quantity > 1) {
      quantity -= 1;
      document.getElementById(this.valueItem).setAttribute('value', `${quantity}`);
    }
  }

  chooseMeal(dish) {
    console.log(1, dish);
    let quantity = Number(document.getElementById(dish.ID).getAttribute('value'));

    let object = {
      "TarifItemID": dish.ID,
      "Quantity": quantity,
      "ParentID": null,
      "Price": dish.Price,
      "ComplexItems": null,
      "Name": dish.ShortName,

    };

    this.items.push(object);
    console.log(2, this.items);



    // "Items": [{
    //   "TarifItemID": 83387,
    //   "Quantity": 1,
    //   "ParentID": null,
    //   "Price": null,
    //   "ComplexItems": null

    // },]

    Swal.fire({
      title: 'Страву замовлено',
      padding: '3em',
      type: 'success',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      showCancelButton: false,
      confirmButtonText: 'Ok',
      backdrop: `
      rgba(0,0,123,0.4)
      url("https://sweetalert2.github.io/images/nyan-cat.gif")
      center left
      no-repeat
    `},
    )
  }

  choose() {
    this.getTablesJason = {
      'token': this.token,
      'TermId': '0987654321'
    };
    console.log(this.getTablesJason);
    this.first = false;
    this.second = false;
    this.third = true;
    this.fourth = false;
    this.sender.getTables(this.getTablesJason).subscribe(tables => {
      this.tables = tables;
      this.places = this.tables.Places;
      console.log(44, this.places);
      this.places.forEach(element => {
        console.log(36, element);
        let id = element.Code;
        this.stolik.push(id);
        setTimeout(() => {
          document.getElementById(id).style.width = String(element.Width + 'px');
          document.getElementById(id).style.height = String(element.Height + 'px');
          document.getElementById(id).style.position = 'relative';
          document.getElementById(id).style.top = String(element.Top + 'px');
          document.getElementById(id).style.left = String(element.Left + 'px');
          document.getElementById(id).style.backgroundColor = 'grey';
        }, 500);
      });
    });
  }

  goToPopUp() {
    if (this.value !== undefined) {
      Swal.fire({
        title: 'Бажаєте замовити страву?',
        padding: '3em',
        type: 'question',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        showCancelButton: true,
        confirmButtonText: 'Так',
        cancelButtonText: 'Ні',
        backdrop: `
        rgba(0,0,123,0.4)
        url("https://sweetalert2.github.io/images/nyan-cat.gif")
        center left
        no-repeat
      `},
      ).then((result) => {
        if (result.value === true) {
          let send = {
            token: this.token,
            SystemCode: '1010'
          };
          this.sender.getMenu(send).subscribe(menu => {
            console.log(2323, menu);
          });
          let send1 = {
            token: this.token,
            SystemCode: '1010',
            GroupMenuID: 82777
          };
          this.sender.getDish(send1).subscribe(menu => {
            console.log(2325, menu);
            this.array = menu;
            this.array1 = this.array.Item;

            this.array1.forEach(element => {
              let testObj = {
                token: this.token,
                ID: element.ID
              }
              //       img.src = 'data:image/png;base64,' + data;
              this.sender.testImg(testObj).subscribe(response => {
                setTimeout(() => {
                  let data = response.toString();
                  console.log(document.getElementsByClassName(element.ID)[0]);
                  console.log(data);

                  document.getElementsByClassName(element.ID)[0].setAttribute('src', `data:image/png;base64,${data}`);
                }, 1000);
              });
            });

          });
          this.first = false;
          this.second = false;
          this.third = false;
          this.fourth = true;
          console.log('true');
        } else if (result.value === undefined) {
          this.bookSimple();
          Swal.fire({
            title: 'Ваш столик зарезервовано',
            type: 'success'
          });
          console.log('false');
        }
      });
    } else {
      Swal.fire({
        type: 'error',
        title: 'Оберіть будь-ласка столик',
      });
    }

  }

  back1() {
    this.first = true;
    this.second = false;
    this.third = false;
    this.fourth = false;
  }

  back2() {
    this.first = false;
    this.second = true;
    this.third = false;
    this.fourth = false;
  }

  back3() {
    this.getTablesJason = {
      'token': this.token,
      'TermId': '0987654321'
    };
    console.log(this.getTablesJason);
    this.sender.getTables(this.getTablesJason).subscribe(tables => {
      this.tables = tables;
      this.places = this.tables.Places;
      console.log(44, this.places);
      this.places.forEach(element => {
        console.log(36, element);
        let id = element.Code;
        this.stolik.push(id);
        setTimeout(() => {
          document.getElementById(id).style.width = String(element.Width + 'px');
          document.getElementById(id).style.height = String(element.Height + 'px');
          document.getElementById(id).style.position = 'relative';
          document.getElementById(id).style.top = String(element.Top + 'px');
          document.getElementById(id).style.left = String(element.Left + 'px');
          document.getElementById(id).style.backgroundColor = 'grey';
        }, 500);
      });
    });
    this.first = false;
    this.second = false;
    this.third = true;
    this.fourth = false;
  }

  back4() {
    this.first = false;
    this.second = false;
    this.third = false;
    this.fourth = true;
    this.fifth = false;
  }

  drinksChoose(event) {
    let send = {
      token: this.token,
      SystemCode: '1010',
      GroupMenuID: 82777
    };
    this.sender.getDish(send).subscribe(menu => {
      console.log(2325, menu);
      this.array = menu;
      this.array1 = this.array.Item.slice(0, 3);
    });
    document.getElementsByClassName('kind-choose')[0].classList.remove('kind-choose');
    event.target.setAttribute('class', 'kind-choose');
    this.drinks = true;
    this.firsCourse = false;
    this.secondCourse = false;
    this.desert = false;
  }

  firstCourseChoose(event) {
    let send = {
      token: this.token,
      SystemCode: '1010',
      GroupMenuID: 82769
    };
    this.sender.getDish(send).subscribe(menu => {
      console.log(2325, menu);
      this.array = menu;
      this.array2 = this.array.Item.slice(0, 3);
    });
    document.getElementsByClassName('kind-choose')[0].classList.remove('kind-choose');
    event.target.setAttribute('class', 'kind-choose');
    this.drinks = false;
    this.firsCourse = true;
    this.secondCourse = false;
    this.desert = false;
  }

  secondCourseChoose(event) {
    let send = {
      token: this.token,
      SystemCode: '1010',
      GroupMenuID: 82795
    };
    this.sender.getDish(send).subscribe(menu => {
      console.log(2325, menu);
      this.array = menu;
      this.array3 = this.array.Item.slice(0, 3);
    });
    document.getElementsByClassName('kind-choose')[0].classList.remove('kind-choose');
    event.target.setAttribute('class', 'kind-choose');
    this.drinks = false;
    this.firsCourse = false;
    this.secondCourse = true;
    this.desert = false;

  }

  desertsChoose(event) {
    let send = {
      token: this.token,
      SystemCode: '1010',
      GroupMenuID: 82769
    };
    this.sender.getDish(send).subscribe(menu => {
      console.log(2325, menu);
      this.array = menu;
      this.array4 = this.array.Item.slice(2, 3);
    });
    document.getElementsByClassName('kind-choose')[0].classList.remove('kind-choose');
    event.target.setAttribute('class', 'kind-choose');
    this.drinks = false;
    this.firsCourse = false;
    this.secondCourse = false;
    this.desert = true;
  }

  conclusion() {
    this.send = {
      "token": this.token,
      "SystemCode": "1010",
      "BillType": 4,
      "OperType": 4,
      "FirstDate": this.firstDate,
      "LastDate": this.secondDate,
      "UserName": "Stephanik",
      "PlaceCode": this.value,
      "EventID": "1",
      "EventName": "1",
      "Items": this.items
    }

    this.items.forEach(element => {
      console.log(34,element)
      console.log(39,element.Price);
     
      this.price += element.Price * element.Quantity;
      
    });
    this.first = false;
    this.second = false;
    this.third = false;
    this.fourth = false;
    this.fifth = true;
    console.log(1, this.send);
  }

  removeOrder(i) {
    console.log(2, i);
    console.log(65, this.items)
    this.price = this.price - this.items[i].Price * this.items[i].Quantity;
    this.items.splice(i, 1);
    console.log(36, this.items[i].Price);
  }


  paymentGo() {
    this.first = false;
    this.second = false;
    this.third = false;
    this.fourth = false;
    this.fifth = false;
    this.six = true;
  }


  // check(event) {
  //   // console.log(event.target.checked);
  //   if (event.target.checked) {
  //     // debugger
  //     if (event.target.attributes.id.nodeValue === "1") {
  //       this.description.nativeElement.value += "Підтвердження дзвінком;";
  //     } else if (event.target.attributes.id.nodeValue === "2") {
  //       this.description.nativeElement.value += " Нагадування на пошту;";
  //     } else if (event.target.attributes.id.nodeValue === "3") {
  //       this.description.nativeElement.value += " Нагадування телефонним дзвінком;";
  //     }
  //   } else {
  //     debugger;
  //     if (event.target.attributes.id.nodeValue === "1") {
  //       this.description.nativeElement.value.replace(/Підтвердження дзвінком;/g, " ")
  //     } else if (event.target.attributes.id.nodeValue === "2") {
  //       this.description.nativeElement.value.replace(" Нагадування на пошту;", " ")
  //     } else if (event.target.attributes.id.nodeValue === "3") {
  //       this.description.nativeElement.value.replace(" Нагадування телефонним дзвінком;", " ")
  //     }
  //   }
  // }
  ngOnInit() {
    // let testObj ={
    //   token : '19460ED2A1BC7873FED2FA0CDFE3358B',
    //   ID : 82851
    // }

    //     this.sender.testImg(testObj).subscribe(response => {
    //       let data = response.toString()
    //       let img = document.createElement('img');
    //       img.src = 'data:image/png;base64,' + data;
    //       document.body.appendChild(img);

    //     })
  }

}
