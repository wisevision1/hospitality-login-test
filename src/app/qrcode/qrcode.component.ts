import { Component, OnInit, OnChanges } from '@angular/core';
import { Input } from '@angular/core';
import { Router } from '@angular/router';
import { Sender } from '../api.service';

@Component({
    selector: 'app-qrcode',
    templateUrl: './qrcode.component.html',
    styleUrls: ['./qrcode.component.scss'],
    providers: [Sender]
})

export class QRCodeComponent implements OnInit {
    public qrdata: string = null;
    @Input() public myAngularxQrCode: string = null;
    constructor(private router: Router, private sender: Sender) {}
    userCode;
    ngOnInit() {
        this.sender.getCabinetInfo(localStorage.getItem('ACCESS_TOKEN')).subscribe( responseCabinetData => {
            this.userCode = responseCabinetData.persondiscount.code;
            this.qrdata = this.userCode; });
    }
}
