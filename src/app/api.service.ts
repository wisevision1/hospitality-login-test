import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class Sender {

  constructor(private httpClient: HttpClient) { }
  // public postRegistrationInfo(req) {
  //   return this.httpClient.post<any>(`https://loyalty.kavalier.com.ua/login`, req);
  //   // 192.168.1.6
  //   // localhost
  //   // loyalty.kavalier.com.ua
  // }

  // public getCabinetInfo(email) {
  //   return this.httpClient.get<any>(`https://loyalty.kavalier.com.ua/cabinet/cabuser/${email}`);
  // }

  // public changeCabinetData(req) {
  //   return this.httpClient.post<any>(`https://loyalty.kavalier.com.ua/cabinet/change/data`, req);
  // }

  // public sendMessage(req) {
  //   return this.httpClient.post<any>(`https://loyalty.kavalier.com.ua/cabinet/send/message`, req);
  // }

  // public getPropositions() {
  //   return this.httpClient.get<any>('https://loyalty.kavalier.com.ua/cab/getthat');
  // }

  // public resetPassword(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/reset', req);
  // }

  // public checkMail(email) {
  //   return this.httpClient.get(`https://loyalty.kavalier.com.ua/serv/mlcheck/${email}`)
  // }

  // public changePass(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/formchangepass', req);
  // }

  //  // User Cabinet field changes
  // public changeFirstName(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/namechange', req);
  // }

  // public changeLastName(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/lastnamechange', req);
  // }

  // public changePatro(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/patrochange', req);
  // }

  // public changePhone(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/mobilechange', req);
  // }

  // public changeEmail(req) {
  //   return this.httpClient.post<any>('https://loyalty.kavalier.com.ua/serv/emailchange', req);
  // }

  // public resPas(email) {
  //   return this.httpClient.get(`https://loyalty.kavalier.com.ua/forgot/${email}`);
  // }




  // public postRegistrationInfo(req) {
  //   return this.httpClient.post<any>(`http://localhost:8011/login`, req);
  //   // 192.168.1.6
  //   // localhost
  //   // loyalty.kavalier.com.ua
  // }

  // public getCabinetInfo(email) {
  //   return this.httpClient.get<any>(`http://localhost:8011/cabinet/cabuser/${email}`);
  // }

  // public changeCabinetData(req) {
  //   return this.httpClient.post<any>(`http://localhost:8011/cabinet/change/data`, req);
  // }

  // public sendMessage(req) {
  //   return this.httpClient.post<any>(`http://localhost:8011/cabinet/send/message`, req);
  // }

  // public getPropositions() {
  //   return this.httpClient.get<any>('http://localhost:8011/cab/getthat');
  // }

  // public resetPassword(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/reset', req);
  // }

  // public checkMail(email) {
  //   return this.httpClient.get(`http://localhost:8011/servio/mlcheck/${email}`);
  // }

  // public changePass(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/formchangepass', req);
  // }

  // // User Cabinet field changes
  // public changeFirstName(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/namechange', req);
  // }

  // public changeLastName(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/lastnamechange', req);
  // }

  // public changePatro(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/patrochange', req);
  // }
  // public changePhone(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/mobilechange', req);
  // }

  // public changeEmail(req) {
  //   return this.httpClient.post<any>('http://localhost:8011/servio/emailchange', req);
  // }
  // public resPas(email) {
  //   return this.httpClient.get(`http://localhost:8011/forgot/${email}`);
  // }

  // FOR test.kavalier.com.ua
  public postRegistrationInfo(req) {
    return this.httpClient.post<any>(`https://test.kavalier.com.ua/login`, req);
    // 192.168.1.6
    // localhost
    // loyalty.kavalier.com.ua
  }

  public getCabinetInfo(email) {
    return this.httpClient.get<any>(`https://test.kavalier.com.ua/cabinet/cabuser/${email}`);
  }

  public changeCabinetData(req) {
    return this.httpClient.post<any>(`https://test.kavalier.com.ua/cabinet/change/data`, req);
  }

  public sendMessage(req) {
    return this.httpClient.post<any>(`https://test.kavalier.com.ua/cabinet/send/message`, req);
  }

  public getPropositions() {
    return this.httpClient.get<any>('https://test.kavalier.com.ua/cab/getthat');
  }

  public resetPassword(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/reset', req);
  }

  public checkMail(email) {
    return this.httpClient.get(`https://test.kavalier.com.ua/serv/mlcheck/${email}`)
  }

  public changePass(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/formchangepass', req);
  }

  // User Cabinet field changes
  public changeFirstName(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/namechange', req);
  }

  public changeLastName(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/lastnamechange', req);
  }

  public changePatro(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/patrochange', req);
  }

  public changePhone(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/mobilechange', req);
  }

  public changeEmail(req) {
    return this.httpClient.post<any>('https://test.kavalier.com.ua/serv/emailchange', req);
  }

  public resPas(email) {
    return this.httpClient.get(`https://test.kavalier.com.ua/forgot/${email}`);
  }

  public terminalAuth(req) {
    return this.httpClient.post(`http://localhost:8011/book/testauth`, req);
  }

  public getTables(req) {
    return this.httpClient.post(`http://localhost:8011/book/placestest`, req);
  }

  public bookTable(req) {
    return this.httpClient.post(`http://localhost:8011/book/testbook`, req);
  }


  public getMenu(req) {
    return this.httpClient.post(`http://localhost:8011/book/testmenu`, req);
  }

  public getDish(req) {
    return this.httpClient.post(`http://localhost:8011/book/testitem`, req);
  }

  public liqPayment(req) {
    return this.httpClient.post(`http://localhost:8011/book/pay`, req);
  }

  public servioPayment(req) {
    return this.httpClient.post(`http://localhost:8011/book/testbillpayment`, req);
  }

  public testImg(req) {
    return this.httpClient.post(`http://localhost:8011/book/testimg`, req);
  }
}
